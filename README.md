EasyCharts is a d3 compiler for advertising data.

It consists of a series of funcitons that populate HTML divisions with SVG coded initially using the popular d3 visualisation package. 

Consistent data structures are key for this. 

Data designated for line/timeseries plots should be compiled as an array of dictionaries, each with keys pertaining to different dimensions of data for a given x value. 

Data for scatter plots should be compiled similarly, however, multiple dictionaries may exist for a given x or y value. 

